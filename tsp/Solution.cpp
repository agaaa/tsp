#include "Solution.h"
#include "Problem.h"
#include "Neighbours.h"
#include "Algorithm.h"
#include "RandomAlgorithm.h"
#include "SteepestAlgorithm.h"
#include "HeuristicAlgorithm.h"
#include "GreedyAlgorithm.h"
#include "SimulatedAnnealing.h"
#include "TabuSearch.h"

#include <ctime>
#include <cfloat>
#include <algorithm>
#include <iomanip>

using namespace std;
using namespace boost::posix_time;

Solution::Solution(Problem *problem, int iteration, Logger *logger, string type, vector<string> algorithms) {
  this->problem = problem;
  this->logger = logger;
  this->neighbours = new Neighbours(this->problem, this->problem->getDimension());
  this->algorithms = algorithms;
  this->iteration = iteration;
  this->type = type;
  this->randomTime = 0.0;
}

//////////////////////////////////////////////

void Solution::compareAlgorithmsSolutions() {
  for(unsigned int i=0; i<this->algorithms.size(); ++i) {
    if(algorithms[i] == "greedy")
      this->greedyLocalSearch();
    else if(algorithms[i] == "steepest")
      this->steepestLocalSearch();
    else if(algorithms[i] == "heuristic")
      this->heuristicsSearch();
    else if(algorithms[i] == "random")
      this->randomSearch();
    else if(algorithms[i] == "annealing")
      this->simulatedAnnealingSearch();
    else if(algorithms[i] == "tabu")
      this->tabuSearch();
  }
}

//////////////////////////////////////////////

void Solution::randomSearch() {
  RandomAlgorithm randomAlg(this->problem, this->logger);
  this->randomTime /= 4;
  randomAlg.setTime(this->randomTime);
  randomAlg.showStatistics(this->iteration, this->type); 
}

//////////////////////////////////////////////

void Solution::steepestLocalSearch() {
  SteepestAlgorithm steepestAlg(this->problem, this->neighbours, this->logger);

  steepestAlg.setType("reverse");
  steepestAlg.showStatistics(this->iteration, this->type);
  this->randomTime += steepestAlg.getTime();
  
  steepestAlg.setType("swap");
  steepestAlg.showStatistics(this->iteration, this->type);
  this->randomTime += steepestAlg.getTime();
}

//////////////////////////////////////////////

void Solution::greedyLocalSearch() {
  GreedyAlgorithm greedyAlg(this->problem, this->neighbours, this->logger);
  
  greedyAlg.setType("reverse");
  greedyAlg.showStatistics(this->iteration, this->type);
  this->randomTime += greedyAlg.getTime();
 
  greedyAlg.setType("swap");
  greedyAlg.showStatistics(this->iteration, this->type);
  this->randomTime += greedyAlg.getTime();
}
          
//////////////////////////////////////////////

void Solution::heuristicsSearch() {
  HeuristicAlgorithm heuristicAlg(this->problem, this->logger);
  heuristicAlg.showStatistics(this->iteration, this->type);
}

//////////////////////////////////////////////

void Solution::simulatedAnnealingSearch() {
  SimulatedAnnealing simulatedAnnealingAlg(this->problem, this->logger);

  simulatedAnnealingAlg.setType("reverse");
  simulatedAnnealingAlg.showStatistics(this->iteration, this->type);
  
  simulatedAnnealingAlg.setType("swap");
  simulatedAnnealingAlg.showStatistics(this->iteration, this->type);
}

//////////////////////////////////////////////

void Solution::tabuSearch() {
  TabuSearch tabuSearchAlg(this->problem, this->neighbours, this->logger);

  tabuSearchAlg.setType("reverse");
  tabuSearchAlg.showStatistics(this->iteration, this->type);
  
  tabuSearchAlg.setType("swap");
  tabuSearchAlg.showStatistics(this->iteration, this->type);
}
