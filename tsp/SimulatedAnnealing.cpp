#include <boost/date_time/posix_time/posix_time.hpp>
#include <cfloat>

#include "Algorithm.h"
#include "Problem.h"
#include "Logger.h"
#include "Neighbours.h"
#include "SimulatedAnnealing.h"

using namespace std;
using namespace boost::posix_time;

SimulatedAnnealing::SimulatedAnnealing(Problem *problem, Logger *logger) : discountFactor(0.9), Tinitial(1.0*problem->getDimension()), temperatureIterations(1*problem->getDimension()) {
  this->name = "annealing";
  this->revOrSwap = "reverse";
  this->metaheuristic = true;
  this->problem = problem;
  this->logger = logger;
  this->solution = new int[this->problem->getDimension()];
}

//////////////////////////////////////////////

void SimulatedAnnealing::solve() {
  this->evaluatedSolutions = 0;

  this->T = this->Tinitial;

  this->getRandomPermutation(this->solution, this->problem->getDimension());
  this->evaluation = this->problem->evaluateSolution(this->solution);
  
  float newEvaluation;
  int i,j,k;
  int iterations = 0;
  int permutationIdx = 0;

  int neighboursCount = this->problem->getDimension() * (this->problem->getDimension()-3);
  int *neighbours = new int[neighboursCount];
  this->getRandomPermutation(neighbours, neighboursCount);
  //for(int i=0; i<neighboursCount; ++i)
  //  neighbours[i] = i;

  int unchanged = 0;
  int maxUnchanged = neighboursCount;

  while(unchanged < maxUnchanged) {

    i = neighbours[permutationIdx] / (this->problem->getDimension() - 3);
    k = neighbours[permutationIdx] - (i * (this->problem->getDimension() - 3));
    j = (i + k + 1) % this->problem->getDimension();

    newEvaluation = this->problem->updateEvaluation(this->revOrSwap, this->evaluation, this->solution, i, j);
    ++this->evaluatedSolutions;

    if(newEvaluation < this->evaluation || ( (rand()*1.0f/INT_MAX) < exp(-(newEvaluation - this->evaluation)/(this->boltzmannConstant*this->T)) && this->evaluation != newEvaluation ) ) {
      
      if(this->revOrSwap == "reverse")
        this->problem->reverse(this->solution, i, j);
      else if(this->revOrSwap == "swap")
        swap(this->solution[i], this->solution[j]);
      
      this->evaluation = newEvaluation;
      this->getRandomPermutation(neighbours, neighboursCount);
      permutationIdx = 0;
      unchanged = 0;
    }
    else {
      permutationIdx = (permutationIdx+1) % neighboursCount;
      ++unchanged;
    }

    // update temperature
    ++iterations;
    if(iterations % temperatureIterations == 0) {
      this->T = this->T * this->discountFactor;
    }

  }
}

//////////////////////////////////////////////

