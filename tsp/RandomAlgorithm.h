#ifndef RANDOM_ALGORITHM
#define RANDOM_ALGORITHM

#include <iostream>

#include "Algorithm.h"
#include "Problem.h"
#include "Logger.h"

class RandomAlgorithm : public Algorithm {

  private:

    float timeLimit;
    int *currentSolution;
    float currentEvaluation;
     
  public:

    // Constructor
    RandomAlgorithm(Problem *problem, Logger *logger);

    void solve();

    void setTime(float miliseconds);
};

#endif // RANDOM_ALGORITHM
