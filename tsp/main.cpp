#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>

#include "Logger.h"
#include "Neighbours.h"
#include "Statistics.h"

using namespace std;

vector<string> initAlgorithms() {
  vector<string> algorithms;
  algorithms.push_back("greedy");
  algorithms.push_back("steepest");
  algorithms.push_back("random");
  algorithms.push_back("heuristic");
  algorithms.push_back("annealing");
  algorithms.push_back("tabu");
  return algorithms;
}

vector<string> initPossibleTypes() {
  vector<string> possibleTypes;
  possibleTypes.push_back("general");
  possibleTypes.push_back("evaluations");
  possibleTypes.push_back("solutions");
  return possibleTypes;
}

vector<int> initIterations(vector<string> types) {
  vector<int> iterations;
  for(unsigned int i=0; i<types.size(); ++i) {
    if(strcmp(types[i].c_str(), "general") == 0) 
      iterations.push_back(10);
    else if(strcmp(types[i].c_str(), "evaluations") == 0) 
      iterations.push_back(300);
    else if(strcmp(types[i].c_str(), "solutions") == 0) 
      iterations.push_back(10);
  }
  return iterations;
}

vector<string> readArgumentsValues(int *i, int argc, char *argv[]) {
  vector<string> values;
  while(*i+1 < argc && argv[*i+1][0] != '-') {
    values.push_back(argv[++(*i)]);
  }
  return values;
}

string joinVectorValues(vector<string> values) {
  string valuesString = "";
  for(vector<string>::iterator it=values.begin(); it<values.end(); ++it)
    valuesString = valuesString + *it + " ";
  return valuesString;
}

bool checkArgumentsCorrectness(vector<string> values, vector<string> possibleValues, string option, Logger logs) {
  if(values.size() == 0) {
    string possibleValuesString = joinVectorValues(possibleValues);
    if(possibleValuesString != "")
      logs.add(ERROR, option + " option requires one or more of the following arguments: " + possibleValuesString);
    else
      logs.add(ERROR, option + " option requires one or more arguments");
    return false;
  }

  if(possibleValues.empty() == false) {
    vector<string> invalidValues;
    copy_if(values.begin(), values.end(), back_inserter(invalidValues),
        [&possibleValues](const string& arg)
        { return (find(possibleValues.begin(), possibleValues.end(), arg) == possibleValues.end());});

    if(invalidValues.size() > 0) {
      string possibleValuesString = joinVectorValues(possibleValues);

      for(vector<string>::iterator it=invalidValues.begin(); it<invalidValues.end(); ++it)
        logs.add(ERROR, "'" + *it + "' is unknown parameter of " + option + " option, possible parameters are following: " + possibleValuesString );

      return false;
    }
  }
  return true;
}

int main(int argc, char *argv[]) {
  Logger logs;
  logs.showLabels();

  vector<string> algorithms = initAlgorithms();
  vector<string> possibleTypes = initPossibleTypes();
  vector<string> types;
  vector<string> sources;
  
  for (int i=1; i<argc; ++i) {
    if (strcmp(argv[i], "--sources") == 0 || strcmp(argv[i], "-s") == 0) {
      sources = readArgumentsValues(&i, argc, argv);
    }
    else if (strcmp(argv[i], "--type") == 0 || strcmp(argv[i], "-t") == 0) {
      types = readArgumentsValues(&i, argc, argv);
    }
  }

  bool typesArgumentsCorrectness = checkArgumentsCorrectness(types, possibleTypes, "--type", logs);
  bool sourcesArgumentsCorrectness = checkArgumentsCorrectness(sources,  vector<string>(), "--source", logs);
  if(typesArgumentsCorrectness == false || sourcesArgumentsCorrectness == false)
    return -1;

  vector<int> iterations = initIterations(types);

  Statistics statistics(logs, sources, types, iterations, algorithms);
  statistics.process();

  return 0;
}
