#ifndef GREEDY_ALGORITHM
#define GREEDY_ALGORITHM

#include <iostream>

#include "Algorithm.h"
#include "Problem.h"
#include "Logger.h"
#include "Neighbours.h"

class GreedyAlgorithm : public Algorithm {

  private:

    Neighbours *neighbours;
    float best_value;

  public:

    // Constructor
    GreedyAlgorithm(Problem *problem, Neighbours *neighbours, Logger *logger);

    void solve();

};

#endif // GREEDY_ALGORITHM
