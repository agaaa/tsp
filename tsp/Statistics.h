#ifndef STATISTICS
#define STATISTICS

#include <iostream>
#include <list>
#include <vector>

#include "Logger.h"

class Statistics {

  private:

    Logger logger;
    std::list<std::string> filesNames;
    std::vector<std::string> types;
    std::vector<std::string> algorithms;
    std::vector<int> iterations;

  public:

    // Constructor
    Statistics(Logger logger, std::vector<std::string> sources, std::vector<std::string> types, std::vector<int> iterations, std::vector<std::string> algorithms);

    // Loads list of files in the given directory. Returns true if succesful.
    bool getFilesNames(std::vector<std::string> sources);

    // Gers statisticts for all files
    void process();

};

#endif //STATISTICS
