#include <boost/date_time/posix_time/posix_time.hpp>
#include <cfloat>

#include "Algorithm.h"
#include "RandomAlgorithm.h"

using namespace std;
using namespace boost::posix_time;

RandomAlgorithm::RandomAlgorithm(Problem *problem, Logger *logger) {
  this->name = "random";
  this->problem = problem;
  this->logger = logger;
  this->solution = new int[this->problem->getDimension()];
  this->currentSolution = new int[this->problem->getDimension()];
}

//////////////////////////////////////////////

void RandomAlgorithm::solve() {
  this->evaluation = FLT_MAX;
  ptime t_start(microsec_clock::local_time());
  float limit = this->timeLimit*1000;
  while((microsec_clock::local_time()-t_start).total_microseconds() < limit) {
    this->getRandomPermutation(this->currentSolution, this->problem->getDimension());
    this->currentEvaluation = this->problem->evaluateSolution(this->currentSolution);
    if(this->currentEvaluation < this->evaluation) {
      this->evaluation = this->currentEvaluation;
      memcpy(this->solution, this->currentSolution, sizeof(int)*this->problem->getDimension());
      //this->solution = this->currentSolution;
    }
  }
}

//////////////////////////////////////////////

void RandomAlgorithm::setTime(float miliseconds) {
  this->timeLimit = miliseconds;
}
