#ifndef STEEPEST_ALGORITHM
#define STEEPEST_ALGORITHM

#include <iostream>

#include "Algorithm.h"
#include "Problem.h"
#include "Logger.h"
#include "Neighbours.h"

class SteepestAlgorithm : public Algorithm {

  private:

    Neighbours *neighbours;

  public:

    // Constructor
    SteepestAlgorithm(Problem *problem, Neighbours *neighbours, Logger *logger);

    void solve();

};

#endif // STEEPEST_ALGORITHM
