#include <ctime>
#include <boost/filesystem.hpp>

#include"Statistics.h"
#include"Solution.h"
#include"Problem.h"

using namespace std;
using namespace boost::filesystem;

Statistics::Statistics(Logger logger, vector<string> sources, vector<string> types, vector<int> iterations, vector<string> algorithms) {
  this->logger = logger;
  this->getFilesNames(sources);
  this->types = types;
  this->algorithms = algorithms;
  this->iterations = iterations;
  srand(unsigned(time(0)));
}

//////////////////////////////////////////////

bool Statistics::getFilesNames(vector<string> sources) {
  for(unsigned int i=0; i<sources.size(); ++i) {
    path p(sources[i]);
    if(is_directory(p)) {
      const path & dir_path(sources[i]);
      directory_iterator end_itr;
      for (directory_iterator itr(dir_path); itr != end_itr; ++itr)
      {
        if (is_regular_file(itr->path())) {
          string current_file = itr->path().string();
          this->filesNames.push_back(current_file);
          //cout << current_file << endl;
        }
      }
    }
    else {
      this->filesNames.push_back(sources[i]);
    }
  }
  return true;
}

//////////////////////////////////////////////

void Statistics::process() {
  for(unsigned int i=0; i<this->types.size(); ++i) {

    for (list<string>::iterator it=(this->filesNames).begin(); it!=(this->filesNames).end(); ++it) {
      //cout << *it << endl;
      logger.add(NAME, *it);
      Problem problem(this->logger, *it);    
      Solution solution(&problem, this->iterations[i], &logger, types[i], this->algorithms);
      solution.compareAlgorithmsSolutions();
    }

  }
}
