#ifndef LOGS
#define LOGS

#include <iostream>
#include <cstdio>

#define NAME_COLOR  "\e[1;34m" //blue
#define ALGORITHM_TYPE_COLOR  "\e[1;36m" //cyan
#define RESULT_COLOR  "\e[1;32m" //green
#define TIME_COLOR  "\e[1;33m" //yellow
#define STEPS_COLOR  "\e[1;35m" //magenta
#define EVALUATED_SOLUTIONS_COLOR  "\e[1;37m" //white
#define INITIAL_VAL_COLOR  "\e[0;34m" //white
#define ERROR_COLOR "\e[1;31m" //red
#define COLOR_END "\e[0m"

enum MessageType { 
  NAME, TYPE, ALGORITHM_TYPE, RESULT, TIME, STEPS, EVALUATED_SOLUTIONS, INITIAL_VAL, ERROR, NEW_LINE_RES, NEW_LINE_EVAL, EVALUATION, SOLUTION
};

class Logger {

  private:
    std::string fileInName;
    std::string type;

    FILE *fileResOut = NULL;
    FILE *fileEvalOut = NULL;
    FILE *fileSolOut = NULL;

    std::string res_dir_name;
    std::string sol_dir_name;
    std::string eval_dir_name;

  public:

    Logger();

    void createDirectoryIfNotExists(std::string path);

    void removeDirectoryContent(std::string path);
    
    void add(MessageType type, std::string message);

    //void openFile(FILE* file);

    //void addResult(std::string alg, float evaluation);
    
    //void addSolution(std::string alg, int* solution, int size, int id=-1);

    void showLabels();

    std::string getFileName(std::string file_path);

};

#endif // LOGS
