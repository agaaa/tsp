#ifndef SOLUTION
#define SOLUTION

#include <iostream>
#include <vector>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "Problem.h"
#include "Logger.h"
#include "Neighbours.h"

class Solution {

  private:

    Problem *problem;
    Logger *logger;
    Neighbours *neighbours;
    int iteration;
    std::string type;
    float randomTime;
    std::vector<std::string> algorithms;

  public:

    // Constructor.
    Solution(Problem *problem, int iteration, Logger *logger, std::string type, std::vector<std::string> algorithms);

    // Compares solutions given by all algorithms.
    void compareAlgorithmsSolutions();

    // Gets random values.
    void randomSearch();

    // Optimizes locally the objective function by steepest algorithm.
    void steepestLocalSearch();

    // Optimizes locally the objective function by greedy algorithm.
    void greedyLocalSearch();

    // Optimizes the objective function using heuristic.
    void heuristicsSearch();

    // Optimizes the objective function using simulated aneealing algorithm.
    void simulatedAnnealingSearch();

    // Optimizes the objective function using tabu search algorithm.
    void tabuSearch();

};

#endif // SOLUTION
