#ifndef ALGORITHM
#define ALGORITHM

#include <iostream>

#include "Problem.h"
#include "Logger.h"

class Algorithm {

  protected:

    int *solution;
    float evaluation;
    bool localSearch = false;
    bool metaheuristic = false;
    float initialEvaluation = 0;
    Problem *problem;
    Logger *logger;
    std::string name;
    unsigned int steps = 0;
    unsigned long long int evaluatedSolutions = 0;
    float time;
    std::string revOrSwap = "";

  public:

    // Gets random permutation
    void getRandomPermutation(int* tab, int max);

    int* getSolution();

    float getInitialSolutionEvaluation();

    float getSolutionEvaluation();

    float compareWithOptimum();

    float getTime();

    void showStatistics(int iteration, std::string type);

    virtual void solve() = 0;

    std::string getStringSolution();
    
    int getRandomIndex(int forbidden = -999);

    int getIndex(int num);

    void setType(std::string type);
};

#endif //ALGORITHM
