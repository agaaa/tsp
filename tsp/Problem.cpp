#include <fstream>
#include <sstream>
#include <cmath>
#include <boost/filesystem.hpp>
#include <algorithm>
#include <iterator>
#include <string>

#include"Problem.h"

using namespace std;
using namespace boost::filesystem;

Problem::Problem(Logger logger, string file_path) {
  this->logger = logger;
  this->load(file_path);
  this->fileName = this->getFileName(file_path);
  this->dimension = this->data.size();
  this->createDistanceMatrix();
  this->data.clear();
}

//////////////////////////////////////////////

int Problem::getDimension() {
  return this->dimension;
}

//////////////////////////////////////////////

float Problem::getDistance(int i, int j) {
  return this->distancesMatrix[i][j];
}

//////////////////////////////////////////////

bool Problem::load(string file_path) {
  ifstream rd_file (file_path);
  if (!rd_file.is_open()) {
    this->logger.add(ERROR, "Cannot open file '" + file_path + "'.");
    return false;
  }

  string line;

  do {
    getline(rd_file, line);
  } while(line != "NODE_COORD_SECTION");

  getline(rd_file, line);
  while(line != "EOF") {
    stringstream ss;
    int id;
    float num1, num2;
    ss << line;
    ss >> id;
    ss >> num1;
    ss >> num2;
    pair<float, float> values = make_pair(num1, num2);
    this->data.push_back(values);
    getline(rd_file, line);
  }

  rd_file.close();
  return true;
}

//////////////////////////////////////////////

string Problem::getFileName(string file_path) {
  size_t found_slash = file_path.find_last_of("/\\");
  size_t found_dot = file_path.find_last_of(".");
  string name = file_path.substr(found_slash+1, found_dot-found_slash-1);
  return name;
}

//////////////////////////////////////////////

bool Problem::loadOptimum(string file_path) {
  ifstream rd_file ("./optimums.txt");
  if (!rd_file.is_open()) {
    this->logger.add(ERROR, "Cannot open file 'optimums.txt'.");
    return false;
  }

  string line, name;
  float value;
  
  do {
    getline(rd_file, line);
    size_t found_colon = line.find(":");
    name = line.substr(0, found_colon-1);
    value = stof(line.substr(found_colon+1));
    if(name == this->getFileName(file_path)) {
      this->optimum_value = value;
      return true;
    }
  } while (line != "EOF");

  return false;
}

//////////////////////////////////////////////

float Problem::getOptimumValue() {
  return this->optimum_value;
}

//////////////////////////////////////////////

float Problem::countDistance(int id1, int id2) {
  int x1 = get<0>(this->data.at(id1));
  int x2 = get<0>(this->data.at(id2));
  int y1 = get<1>(this->data.at(id1));
  int y2 = get<1>(this->data.at(id2));
  return sqrt(pow(x1-x2,2) + pow(y1-y2,2));
}

//////////////////////////////////////////////

void Problem::createDistanceMatrix() {
  this->distancesMatrix = new float*[this->dimension];
  for(int i=0; i<this->dimension; ++i) {
    this->distancesMatrix[i] = new float[this->dimension];
  }

  float distance;
  for(int i=0; i<this->dimension; ++i) {
    for(int j=0; j<i; ++j) {
      distance = this->countDistance(i,j);
      this->distancesMatrix[i][j] = distance;
      this->distancesMatrix[j][i] = distance;
    }
    this->distancesMatrix[i][i] = 0.0f;
  }
}

//////////////////////////////////////////////

float Problem::evaluateSolution(int *solution) {
  float distance = this->distancesMatrix[solution[this->dimension-1]][solution[0]];
  for(int i=1; i<this->dimension; ++i) {
    distance += this->distancesMatrix[solution[i-1]][solution[i]];
  }
  return distance;
}

//////////////////////////////////////////////

float Problem::updateEvaluation(std::string type, float prev_value, int *solution, int id1, int id2) {
  if(type == "reverse")
    return this->updateReverseEvaluation(prev_value, solution, id1, id2);
  else if(type == "swap")
    return this->updateSwapEvaluation(prev_value, solution, id1, id2);
  return -1.0;
}

//////////////////////////////////////////////

float Problem::updateSwapEvaluation(float prev_value, int *solution, int id1, int id2) {
  float result = prev_value;

  int prev_id1 = id1 - 1;
  int prev_id2 = id2 - 1;
  int next_id1 = id1 + 1;
  int next_id2 = id2 + 1;
  if(prev_id1 < 0) prev_id1 = this->dimension - 1;
  if(prev_id2 < 0) prev_id2 = this->dimension - 1;
  if(next_id1 == this->dimension) next_id1 = 0;
  if(next_id2 == this->dimension) next_id2 = 0;

  if(id2 == (id1 + 1)%this->dimension) {
    result -= this->distancesMatrix[solution[prev_id1]][solution[id1]];
    result -= this->distancesMatrix[solution[id2]][solution[next_id2]];
    result += this->distancesMatrix[solution[prev_id1]][solution[id2]];
    result += this->distancesMatrix[solution[id1]][solution[next_id2]];
  }
  else if(id1 == (id2 + 1)%this->dimension) {
    result -= this->distancesMatrix[solution[prev_id2]][solution[id2]];
    result -= this->distancesMatrix[solution[id1]][solution[next_id1]];
    result += this->distancesMatrix[solution[prev_id2]][solution[id1]];
    result += this->distancesMatrix[solution[id2]][solution[next_id1]];
  }
  else {
    result -= this->distancesMatrix[solution[prev_id1]][solution[id1]];
    result -= this->distancesMatrix[solution[id1]][solution[next_id1]];
    result -= this->distancesMatrix[solution[prev_id2]][solution[id2]];
    result -= this->distancesMatrix[solution[id2]][solution[next_id2]];

    result += this->distancesMatrix[solution[prev_id1]][solution[id2]];
    result += this->distancesMatrix[solution[id2]][solution[next_id1]];
    result += this->distancesMatrix[solution[prev_id2]][solution[id1]];
    result += this->distancesMatrix[solution[id1]][solution[next_id2]];
  }
  
  return result;
}

//////////////////////////////////////////////

float Problem::updateReverseEvaluation(float prev_value, int *solution, int id1, int id2) {
  if(id1 == (id2+1)%this->dimension)
    return prev_value;

  float result = prev_value;

  int prev_id1 = id1 - 1;
  int next_id2 = id2 + 1;
  if(prev_id1 < 0) prev_id1 = this->dimension - 1;
  if(next_id2 == this->dimension) next_id2 = 0;

  result -= this->distancesMatrix[solution[prev_id1]][solution[id1]];
  result -= this->distancesMatrix[solution[id2]][solution[next_id2]];
  result += this->distancesMatrix[solution[prev_id1]][solution[id2]];
  result += this->distancesMatrix[solution[id1]][solution[next_id2]];
  
  return result;
}

//////////////////////////////////////////////

void Problem::reverse(int *buffer, int id1, int id2) {
  int distance = (id2 - id1 + this->dimension) % this->dimension;
  for(int i=0; 2*i<distance; ++i) {
    swap(buffer[(id1+i)%this->dimension], buffer[(id2-i+this->dimension)%this->dimension]);
  }
}

