#ifndef PROBLEM
#define PROBLEM

#include <iostream>
#include <vector>
#include <utility>

#include "Logger.h"

class Problem {

  private:

    Logger logger;
    std::vector<std::pair<float, float>> data;
    int dimension;
    float **distancesMatrix;
    float optimum_value;
    std::string fileName;

  public:

    // Constructor
    Problem(Logger logger, std::string file_path);
    
    // Gets dimension value
    int getDimension();

    // Gets distance between 2 elements
    float getDistance(int i, int j);

    // Loads problem instance from the given file. Returns true if succesful.
    bool load(std::string file_path);
    
    // Loads optimum value and instance.
    bool loadOptimum(std::string file_path);

    // Counts distance between 2 nodes
    float countDistance(int id1, int id2);

    // Counts distances for all nodes
    void createDistanceMatrix();

    // Evaluates given solution
    float evaluateSolution(int *solution);

    // Updates solution evaluation
    float updateEvaluation(std::string type, float prev_value, int *solution, int id1, int id2);

    // Updates solution evaluation - swap
    float updateSwapEvaluation(float prev_value, int *solution, int id1, int id2);

    // Updates solution evaluation - reverse
    float updateReverseEvaluation(float prev_value, int *solution, int id1, int id2);

    void reverse(int *buffer, int id1, int id2);

    std::string getFileName(std::string file_path);

    float getOptimumValue();
};

#endif //PROBLEM
