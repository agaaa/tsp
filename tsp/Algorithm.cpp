#include <algorithm>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <cstdlib>

#include "Algorithm.h"

using namespace std;
using namespace boost::posix_time;

void Algorithm::getRandomPermutation(int* tab, int max) {
  for(int i=0; i<max; ++i) {                                           
    tab[i] = i;                                                       
  }                                                                   
  random_shuffle(&tab[0], &tab[max]);                                  
  //for(int i=0; i<max; ++i) cout << tab[i]; cout << endl;
}

//////////////////////////////////////////////

int* Algorithm::getSolution() {
  return this->solution;
}

//////////////////////////////////////////////

float Algorithm::getSolutionEvaluation() {
  return this->evaluation;
}

//////////////////////////////////////////////

float Algorithm::getInitialSolutionEvaluation() {
  return this->initialEvaluation;
}

//////////////////////////////////////////////

float Algorithm::compareWithOptimum() {
  return this->evaluation - this->problem->getOptimumValue() / this->problem->getOptimumValue();
}
    
//////////////////////////////////////////////

float Algorithm::getTime() {
  return this->time;
}

//////////////////////////////////////////////

void Algorithm::showStatistics(int iteration, string type) {
  this->logger->add(TYPE, type);
  
  string alg = this->name;
  if(this->revOrSwap != "")
    alg = alg + "-" + this->revOrSwap;
  this->logger->add(ALGORITHM_TYPE, alg);
  
  if(type == "general") {
    for(int i=0; i<iteration; ++i) {
      ptime t1(microsec_clock::local_time());
      this->solve();
      ptime t2(microsec_clock::local_time());
      this->logger->add(RESULT, to_string(this->evaluation));
      this->time = (t2-t1).total_microseconds()*1.0/1000;
      this->logger->add(TIME, to_string(this->time));
      if(this->localSearch == true) {
        this->logger->add(EVALUATED_SOLUTIONS, to_string(this->evaluatedSolutions));
        this->logger->add(STEPS, to_string(this->steps));
      }
      else if(this->metaheuristic == true) {
        this->logger->add(EVALUATED_SOLUTIONS, to_string(this->evaluatedSolutions));
      }
      this->logger->add(NEW_LINE_RES, "");
    }
  }

  else if(type == "solutions") {
    for(int i=0; i<iteration; ++i) {
      this->solve();
    }
    this->logger->add(SOLUTION, this->getStringSolution());
  }
  else if(type == "evaluations" && this->name != "random" && this->name != "heuristic") {
    for(int i=0; i<iteration; ++i) {
      this->solve();
      this->logger->add(EVALUATION, to_string(this->evaluation));
      if(this->localSearch == true) {
        this->logger->add(INITIAL_VAL, to_string(this->initialEvaluation));
      }
      //this->logger->add(NEW_LINE_EVAL, "");
    }

  }
}


//////////////////////////////////////////////

string Algorithm::getStringSolution() {
  string strSol = ""; 
  for(int i=0; i<this->problem->getDimension(); ++i) {
    strSol += to_string(this->solution[i]);
    strSol += "\n";
  }
  return strSol;
}

//////////////////////////////////////////////

void Algorithm::setType(std::string type) {
  this->revOrSwap = type;
}
