#ifndef NEIGHBOURS
#define NEIGHBOURS

#include <iostream>

#include "Problem.h"

class Neighbours {

  private:

    Problem *problem;
    int dimensions;
    float best_value;
    int evaluatedSolutions;

  public:

    Neighbours(Problem *problem, int dimensions);

    bool check(float initial_value, int *instance, bool first, std::string type);
    bool checkReverse(float initial_value, int *instance, bool first);
    bool checkSwap(float initial_value, int *instance, bool first);

    std::vector<std::pair<std::pair<int, int>, float>> getBestTabuNeighbours(float initial_value, float best_value, int *instance, int **tabu, int step, int tabuListLength, unsigned long long int *evaluatedSolutions, std::string revOrSwap);

    std::vector<std::pair<std::pair<int, int>, float>> updateBestTabuNeighbours(float initial_value, float best_value, int *instance, int **tabu, int step, int tabuListLength, std::vector<std::pair<std::pair<int, int>, float>> currentNeighbours, unsigned long long int *evaluatedSolutions, std::string revOrSwap);

    float get_best_value();

    void showNeighbour(int *tab);

    int getEvaluatedSolutions();

};

#endif //NEIGHBOURS
