#include "Neighbours.h"

#include <boost/bind.hpp>
#include <algorithm>
#include <cfloat>
#include <cstdio>
#include <cstring>
#include <vector>
#include <utility>

using namespace std;

Neighbours::Neighbours(Problem *problem, int dimensions) {
  this->problem = problem;
  this->dimensions = dimensions;
}

//////////////////////////////////////////////

bool Neighbours::check(float initial_value, int *instance, bool first, string type) {
  if(type == "reverse")
    return this->checkReverse(initial_value, instance, first);
  else if(type == "swap")
    return this->checkSwap(initial_value, instance, first);
  return false;
}

//////////////////////////////////////////////

bool Neighbours::checkReverse(float initial_value, int *instance, bool first) {
  this->best_value = initial_value;
  float value;
  int best_i = -1, best_j = -1, j;
  this->evaluatedSolutions = 0;

  for(int i=0; i<dimensions; ++i) {
    for(int k=1; k<dimensions-2; ++k) {
      ++evaluatedSolutions;

      j = (i + k) % dimensions;
      value = this->problem->updateEvaluation("reverse", initial_value, instance, i, j);
      if(first == true) { // greedy
        if(value < initial_value) {
          this->best_value = value;
          this->problem->reverse(instance, i, j);
          return true;
        }
      }
      else { // steepest
        if(value < this->best_value) {
          this->best_value = value;
          best_i = i;
          best_j = j;
        }
      }
    }
  }
  if(first == false && this->best_value < initial_value) {
    this->problem->reverse(instance, best_i, best_j);
    return true;
  }

  this->best_value = initial_value;
  return false;
}

//////////////////////////////////////////////

bool Neighbours::checkSwap(float initial_value, int *instance, bool first) {
  this->best_value = initial_value;
  float value;
  int best_i = -1, best_j = -1;

  for(int i=0; i<dimensions-1; ++i) {
    for(int j=i+1; j<dimensions; ++j) {
      value = this->problem->updateEvaluation("swap", initial_value, instance, i, j);
      if(first == true) {
        if(value < initial_value) {
          this->best_value = value;
          swap(instance[i], instance[j]);
          return true;
        }
      }
      else {
        if(value < this->best_value) {
          this->best_value = value;
          best_i = i;
          best_j = j;
        }
      }
    }
  }
  if(first == false && this->best_value < initial_value) {
    swap(instance[best_i], instance[best_j]);
    return true;
  }

  this->best_value = initial_value;
  return false;
}

//////////////////////////////////////////////

void Neighbours::showNeighbour(int *tab) {
  for(int i=0; i<this->dimensions; ++i) {
    cout << tab[i] << " ";
  }
  cout << endl;
}

//////////////////////////////////////////////

float Neighbours::get_best_value() {
  return this->best_value;
}

//////////////////////////////////////////////

int Neighbours::getEvaluatedSolutions() {
  return this->evaluatedSolutions;
}

//////////////////////////////////////////////

vector<pair<pair<int,int>,float>> Neighbours::getBestTabuNeighbours(float initial_value, float best_value, int *instance, int **tabu, int step, int tabuListLength, unsigned long long int *evaluatedS, string revOrSwap) {
  float value;
  int j;

  vector<pair<pair<int, int>, float>> best;

  for(int i=0; i<dimensions; ++i) {
    for(int k=1; k<dimensions-2; ++k) {
      ++*evaluatedS;

      j = (i + k) % dimensions;

      value = this->problem->updateEvaluation(revOrSwap, initial_value, instance, i, j);
      if(value < best_value || (value < initial_value && tabu[i][j] <= step - tabuListLength))  {
        best.push_back(make_pair(make_pair(i,j), value));
        //best.push_back(make_pair(i*(this->problem->getDimension()-3)+k-1, value));
      }
    }
  }
  
  sort(best.begin(), best.end(), 
      boost::bind(&std::pair<std::pair<int, int>, float>::second, _1) <
      boost::bind(&std::pair<std::pair<int, int>, float>::second, _2));

  return best;
}

//////////////////////////////////////////////

vector<pair<pair<int, int>, float>> Neighbours::updateBestTabuNeighbours(float initial_value, float best_value, int *instance, int **tabu, int step, int tabuListLength, vector<pair<pair<int, int>, float>> currentNeighbours, unsigned long long int *evaluatedS, string revOrSwap) {
  
  vector<pair<pair<int, int>, float>> newBest;
  float value;
  
  for(unsigned int i=0; i<currentNeighbours.size(); ++i) {
    ++*evaluatedS;
    value = this->problem->updateEvaluation(revOrSwap, initial_value, instance, currentNeighbours[i].first.first, currentNeighbours[i].first.second);
    if(value < best_value || (value < initial_value && tabu[currentNeighbours[i].first.first][currentNeighbours[i].first.second] <= step - tabuListLength))  {
      newBest.push_back(make_pair(make_pair(currentNeighbours[i].first.first, currentNeighbours[i].first.second), value));
    }
  }

  sort(newBest.begin(), newBest.end(), 
      boost::bind(&std::pair<std::pair<int, int>, float>::second, _1) <
      boost::bind(&std::pair<std::pair<int, int>, float>::second, _2));
  
  if(newBest.size() == 0)
    return this->getBestTabuNeighbours(initial_value, best_value, instance, tabu, step, tabuListLength, evaluatedS, revOrSwap);
 
  return newBest;
}
