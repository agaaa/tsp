#include "Algorithm.h"
#include "HeuristicAlgorithm.h"

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cfloat>

using namespace std;

HeuristicAlgorithm::HeuristicAlgorithm(Problem *problem, Logger *logger) {
  this->name = "heuristic";
  this->problem = problem;
  this->logger = logger;
  this->solution = new int[this->problem->getDimension()];
  this->visited = new bool[this->problem->getDimension()];
}

//////////////////////////////////////////////

void HeuristicAlgorithm::solve() {
  memset(this->visited, false, this->problem->getDimension());

  int id = rand() % this->problem->getDimension();
  this->solution[0] = id;
  this->visited[id] = true;
  
  int previous = id;
  for(int i=1; i<this->problem->getDimension(); ++i) {
    id = this->getNearest(previous);
    this->solution[i] = id;
    this->visited[id] = true; 
    previous = id;
  }
  this->evaluation = this->problem->evaluateSolution(solution);
}

//////////////////////////////////////////////

int HeuristicAlgorithm::getNearest(int id) {
  float min_distance = FLT_MAX;
  int min_id = -1;
  for(int i=0; i<this->problem->getDimension(); ++i) {
    if(this->problem->getDistance(id, i) < min_distance && this->visited[i] == false) {
      min_distance = this->problem->getDistance(id, i);
      min_id = i;
    }
  }
  return min_id;
}
