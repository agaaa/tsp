#ifndef TABU
#define TABU

#include <iostream>

#include "Algorithm.h"
#include "Problem.h"
#include "Logger.h"

class TabuSearch : public Algorithm {

  private:
    Neighbours *neighbours;
    int **tabuArray;
    const int tabuListLength;

    int *currentSolution;
    float currentEvaluation;

  public:

    // Constructor
    TabuSearch(Problem *problem, Neighbours *neighbours, Logger *logger);

    void solve();

};

#endif // TABU
