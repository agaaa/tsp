#include "Algorithm.h"
#include "SteepestAlgorithm.h"

#include <stdio.h>
#include <string.h>
#include <cfloat>

using namespace std;

SteepestAlgorithm::SteepestAlgorithm(Problem *problem, Neighbours *neighbours, Logger *logger) {
  this->name = "steepest";
  this->revOrSwap = "reverse";
  this->localSearch = true;
  this->problem = problem;
  this->logger = logger;
  this->neighbours = neighbours;
  this->solution = new int[this->problem->getDimension()];
}

//////////////////////////////////////////////

void SteepestAlgorithm::solve() {
  this->getRandomPermutation(this->solution, this->problem->getDimension());
  this->evaluation = this->problem->evaluateSolution(solution);
  this->initialEvaluation = this->evaluation;
  this->evaluatedSolutions = 1;

  while(this->neighbours->check(this->evaluation, this->solution, false, this->revOrSwap) == true) {
    this->evaluation = this->neighbours->get_best_value();
    ++steps;
    this->evaluatedSolutions += this->neighbours->getEvaluatedSolutions();
  }
}

