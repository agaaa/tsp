#ifndef HEURISTIC_ALGORITHM
#define HEURISTIC_ALGORITHM

#include <iostream>

#include "Algorithm.h"
#include "Problem.h"
#include "Logger.h"
#include "Neighbours.h"

class HeuristicAlgorithm : public Algorithm {

  private:

    bool *visited;

  public:

    // Constructor
    HeuristicAlgorithm(Problem *problem, Logger *logger);

    void solve();

    int getNearest(int id);
};

#endif // HEURISTIC_ALGORITHM
