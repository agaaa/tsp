#include <boost/date_time/posix_time/posix_time.hpp>
#include <cfloat>

#include "Algorithm.h"
#include "Problem.h"
#include "Logger.h"
#include "Neighbours.h"
#include "TabuSearch.h"

using namespace std;
using namespace boost::posix_time;

TabuSearch::TabuSearch(Problem *problem, Neighbours *neighbours, Logger *logger) : tabuListLength(0.1*this->problem->getDimension()) {
  this->name = "tabu";
  this->revOrSwap = "reverse"; 
  this->metaheuristic = true;
  this->problem = problem;
  this->neighbours = neighbours;
  this->logger = logger;
  this->solution = new int[this->problem->getDimension()];
  this->currentSolution = new int[this->problem->getDimension()];

  this->tabuArray = new int*[this->problem->getDimension()];
  for(int i=0; i<this->problem->getDimension(); ++i)
    this->tabuArray[i] = new int[this->problem->getDimension()];
  
  for(int i=0; i<this->problem->getDimension(); ++i)
    for(int j=0; j<this->problem->getDimension(); ++j)
      this->tabuArray[i][j] = -tabuListLength;
  //memset(this->tabuArray, 0, sizeof(this->tabuArray[0][0])*this->problem->getDimension()*this->problem->getDimension());

}

//////////////////////////////////////////////

void TabuSearch::solve() {
  this->evaluatedSolutions = 0;
  this->getRandomPermutation(this->solution, this->problem->getDimension());
  memcpy(this->currentSolution, this->solution, sizeof(int)*this->problem->getDimension());
  this->evaluation = this->problem->evaluateSolution(this->solution);
  this->currentEvaluation = this->evaluation;

  int step = 0;
  int i, j;
  vector<pair<pair<int, int>, float>> bestNeighbours;
  bool continuation = true;

  while(continuation == true) {

    bestNeighbours = this->neighbours->updateBestTabuNeighbours(this->currentEvaluation, this->evaluation, this->currentSolution, this->tabuArray, step, tabuListLength, bestNeighbours, &this->evaluatedSolutions, this->revOrSwap);

    if(bestNeighbours.size() == 0) {
      continuation = false;
    }
    else {
      i = bestNeighbours[0].first.first;
      j = bestNeighbours[0].first.second;

      this->currentEvaluation = this->problem->updateEvaluation(this->revOrSwap, this->currentEvaluation, this->currentSolution, i, j);
      if(this->revOrSwap == "reverse")
        this->problem->reverse(this->currentSolution, i, j);
      else if(this->revOrSwap == "swap")
        swap(this->currentSolution[i], this->currentSolution[j]);

      if(this->currentEvaluation < this->evaluation) {
        memcpy(this->solution, this->currentSolution, sizeof(int)*this->problem->getDimension());   
        this->evaluation = currentEvaluation;
      }
      this->tabuArray[i][j] = step;
      ++step; 

    }
  }
}
