#ifndef SIMULATED_ANNEALING
#define SIMULATED_ANNEALING

#include <iostream>

#include "Algorithm.h"
#include "Problem.h"
#include "Logger.h"

class SimulatedAnnealing : public Algorithm {

  private:

    const float discountFactor;
    const float Tinitial;
    //const float Tmin;
    const int temperatureIterations;
    const double boltzmannConstant = 1.3806503e-23;

    float T;
    int *neighbourSolution;
    float neighbourEvaluation;

    Neighbours *neighbours;
    
  public:

    // Constructor
    SimulatedAnnealing(Problem *problem, Logger *logger);

    void solve();

};

#endif // SIMULATED_ANNEALING
