#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <boost/filesystem.hpp>

#include "Logger.h"

using namespace std;

Logger::Logger() {
  this->res_dir_name = "./results";
  this->sol_dir_name = "./solutions";
  this->eval_dir_name = "./evaluations";

  this->createDirectoryIfNotExists(this->res_dir_name);
  this->createDirectoryIfNotExists(this->sol_dir_name);
  this->createDirectoryIfNotExists(this->eval_dir_name);

  /*
  this->removeDirectoryContent(this->res_dir_name);
  this->removeDirectoryContent(this->sol_dir_name);
  this->removeDirectoryContent(this->eval_dir_name);
  */
}

//////////////////////////////////////////////
    
void Logger::createDirectoryIfNotExists(std::string path) {
  boost::filesystem::path dir(path);
  if(boost::filesystem::create_directory(dir)) {
    this->add(ERROR, "Cannot create directory '" + path + "'");
  }
}

//////////////////////////////////////////////
    
void Logger::removeDirectoryContent(std::string path) {
  boost::filesystem::path path_to_remove(path);
  for (boost::filesystem::directory_iterator end_dir_it, it(path_to_remove); it!=end_dir_it; ++it) {
    boost::filesystem::remove_all(it->path());
  }
}

//////////////////////////////////////////////

void Logger::add(MessageType type, string message) {
  string directory_path;
  switch(type) {
    case NAME:
      cout << NAME_COLOR << message << COLOR_END << endl;
      this->fileInName = this->getFileName(message);
      break;

    case TYPE:
      this->type = message;
      break;

    case ALGORITHM_TYPE:
      cout << ALGORITHM_TYPE_COLOR << message << COLOR_END << endl;

      if(this->type == "general") { 
        if(fileResOut != NULL) fclose(fileResOut);
        directory_path = this->res_dir_name + "/" + this->fileInName + "." + message + ".out";
        this->fileResOut = fopen(directory_path.c_str(), "w");
        if(fileResOut == NULL) 
          this->add(ERROR, ("Cannot open file '" + directory_path + "'").c_str());
      }
      else if(this->type == "evaluations") {
        if(fileEvalOut != NULL) fclose(fileEvalOut);
      directory_path = this->eval_dir_name + "/" + this->fileInName + "." + message + ".out";
      this->fileEvalOut = fopen(directory_path.c_str(), "w");
      if(fileEvalOut == NULL) 
        this->add(ERROR, ("Cannot open file '" + directory_path +"'").c_str());
  }
  else if(this->type == "solutions") {
    if(fileSolOut != NULL) fclose(fileSolOut);
    directory_path = this->sol_dir_name + "/" + this->fileInName + "." + message + ".out";
    this->fileSolOut = fopen(directory_path.c_str(), "w");
    if(fileSolOut == NULL) 
      this->add(ERROR, ("Cannot open file '" + directory_path +"'").c_str());
  }
  break;

    case RESULT:
      cout << "\t" << RESULT_COLOR << message << COLOR_END;
      if(fileResOut != NULL) {
        fputs("\t", fileResOut);
        fputs(message.c_str(), fileResOut);
      } 
      break;
    case TIME:
      cout << "\t" << TIME_COLOR << message << COLOR_END;
      if(fileResOut != NULL) {
        fputs("\t", fileResOut);
        fputs(message.c_str(), fileResOut);
      }
      break;
    case STEPS:
      cout << "\t" << STEPS_COLOR << message << COLOR_END;
      if(fileResOut != NULL) {
        fputs("\t", fileResOut);
        fputs(message.c_str(), fileResOut);
      }
      break;
    case EVALUATED_SOLUTIONS:
      cout << "\t" << EVALUATED_SOLUTIONS_COLOR << message << COLOR_END;
      if(fileResOut != NULL) {
        fputs("\t", fileResOut);
        fputs(message.c_str(), fileResOut);
      }
      break;
    case INITIAL_VAL:
      cout << "\t" << INITIAL_VAL_COLOR << message << COLOR_END;
      if(fileEvalOut != NULL) {
        fputs("\t", fileEvalOut);
        fputs(message.c_str(), fileEvalOut);
      }
      break;
    case ERROR:
      cout << endl << ERROR_COLOR << message << COLOR_END << endl << endl;
      break;
    case NEW_LINE_RES:
      cout << endl;
      if(fileResOut != NULL) {
        fputs("\n", fileResOut);
      }
      break;
    case NEW_LINE_EVAL:
      cout << endl;
      if(fileEvalOut != NULL) {
        fputs("\n", fileEvalOut);
      }
      break;
    case SOLUTION:
      //cout << message << endl;
      if(fileSolOut != NULL) {
        fputs(message.c_str(), fileSolOut);
      }
      else
        cout << "Cannot open file" << endl;
      break;
    case EVALUATION:
      cout << "\t" << message;
      if(fileEvalOut != NULL) {
        fputs("\t", fileEvalOut);
        fputs(message.c_str(), fileEvalOut);
      }
      else
        cout << "Cannot open file" << endl;
      break;
  }
}

//////////////////////////////////////////////

void Logger::showLabels() {
  cout << "-----------------------------------" << endl;
  cout << NAME_COLOR << "file name" << COLOR_END << endl;
  cout << ALGORITHM_TYPE_COLOR << "algorithm" << COLOR_END << endl;
  cout << RESULT_COLOR << "result" << COLOR_END << endl;
  cout << TIME_COLOR << "time" << COLOR_END << endl;
  cout << STEPS_COLOR << "steps" << COLOR_END << endl;
  cout << EVALUATED_SOLUTIONS_COLOR << "evaluated solutions" << COLOR_END << endl;
  cout << INITIAL_VAL_COLOR << "initial solution evaluation" << COLOR_END << endl;
  cout << ERROR_COLOR << "errors" << COLOR_END << endl;
  cout << "-----------------------------------" << endl;
}

//////////////////////////////////////////////

string Logger::getFileName(string file_path) {
  size_t found_slash = file_path.find_last_of("/\\");
  size_t found_dot = file_path.find_last_of(".");
  string name = file_path.substr(found_slash+1, found_dot-found_slash-1);
  return name;
}

