# README - Travelling salesman problem #
Comparison of random, greedy, steepest, heuristic, tabu search and simulated annealing algorithms.
  
## BUILDING ##
cmake.
make

## RUNNING ##
./tsp -s **source_file** -t **type_of_comparison**