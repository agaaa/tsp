from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.pyplot import *
import numpy as np
from matplotlib.font_manager import FontProperties

class OptDist:

    def __init__(self, name, algorithms, data, optimum_value):
        self.name = name
        self.algorithms = algorithms
        self.data = data
        self.optimum = optimum_value
        self.colors = ["r", "g", "b", "c", "m", "y"]
        self.iterations = len(self.data[self.algorithms[0]][0])
        
        for t in ["mean", "max", "min"]:
            self.create_plot(t)

    def create_plot(self, graph_type):
        fontP = FontProperties()
        fontP.set_size('small')
        
        fig = plt.figure()
        ax = plt.subplot(111)
        box = ax.get_position()
        #ax.set_position([box.x0, box.y0, box.width * 0.55, box.height])
        ax.set_position([box.x0, box.y0 + box.height * 0.25,
                             box.width, box.height * 0.8])

        plt.yscale('log')

        # optimum  
        optimum_list = [self.optimum for i in range(self.iterations)]
        ax.plot(optimum_list, "k", linewidth=2.0, label="Optimum value") 

        # for all algorithms
        for i in range(len(self.algorithms)):
            color = self.colors[i%len(self.colors)]

            ax.plot(self.data[self.algorithms[i]][0], color + 'o', label=self.algorithms[i] + " - values") 

            l = self.create_list(graph_type, self.data[self.algorithms[i]][0])
            ax.plot(l, color, label=self.algorithms[i]+" - "+graph_type) 

        plt.title("Odleglosc od optimum (" + graph_type + ") - " + self.name)
        plt.xlabel('Iteracja')
        plt.ylabel('Wartosc funkcji celu (skala logarytmiczna)')

        #ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.13),
                          fancybox=True, shadow=True, ncol=3, prop = fontP, numpoints=1)

        #plt.show()
        plt.savefig("./pictures/distance_" + graph_type + "_" + self.name + ".pdf")

    def create_list(self, graph_type, d):
        if graph_type == "mean":
            value = np.mean(d)
        elif graph_type == "max":
            value = max(d)
        elif graph_type == "min":
            value = min(d)
        else:
            print "create_list - invalid argument"
            value = 0.0
        return [value for j in range(self.iterations)]
