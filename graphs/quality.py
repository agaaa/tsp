from matplotlib import pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties

class Quality:

    def __init__(self, name, algorithms, data, optimum):
        self.name = name
        self.algorithms = algorithms
        self.data = data
        self.iterations = len(self.data[self.algorithms[0]][0])
        self.optimum = optimum
        
        self.create_plot()

    def create_plot(self):
        fontP = FontProperties()
        fontP.set_size('small')

        fig = plt.figure()
        ax = fig.add_subplot(111)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + 0.2*box.height, box.width, 0.8*box.height])

        for a in self.algorithms:
            quality_initial, quality_final = [], []
            for i in range(self.iterations):
                quality_initial.append(self.optimum*100.0 / self.data[a][1][i])
                quality_final.append(self.optimum*100.0 / self.data[a][0][i])

            ax.plot(quality_initial, quality_final, marker='o', linewidth=0, markersize=2, markeredgewidth=0, label=a)

        plt.xlabel('Jakosc rozwiazania poczatkowego [%]')
        plt.ylabel('Jakosc rozwiazania koncowego [%]')
        
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), fancybox=True, shadow=True, ncol=2, prop = fontP, numpoints=1)
        
        plt.title('Zaleznosc miedzy poczatkowa i koncowa jakoscia - ' + self.name)
        plt.savefig("./pictures/initial_final_" + self.name + ".pdf")

        plt.cla()   # Clear axis
        plt.clf()   # Clear figure
        plt.close() # Close a figure window

