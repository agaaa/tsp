#!/usr/bin/env python

import sys

from files import FilesOperations
from distance import Distance
from algorithm_time import AlgTime
from efficiency import Efficiency
from steps import Steps
from sol_num import SolutionsNum
from quality import Quality
from paths import Paths
from restarts import Restarts

def main():
    if len(sys.argv) < 2:
        print '''Usage: python main.py dist|time|eff|steps|snum|qcomp|path|rest|all'''
    else:
        results_directory = "results"
        optimums_directory = "optimums"
        solutions_directory = "solutions"
        evaluations_directory = "evaluations"

        optimums_file = "optimums.txt"
        
        algorithms = dict()
        for a in [ "greedy-reverse", "greedy-swap", "steepest-reverse", "steepest-swap" ]:
            algorithms[a] = "GS"
        for a in [ "annealing-reverse", "annealing-swap", "tabu-reverse", "tabu-swap" ]:
            algorithms[a] = "MH"
        algorithms[ "heuristic" ] = "H"
        algorithms[ "random" ] = "R"
       
        markers = dict()
        markers["GS"] = 'o'
        markers["MH"] = '^'
        markers["H"] = '*'
        markers["R"] = 'x'

        lines = dict()
        lines["GS"] = '-'
        lines["MH"] = '--'
        lines["H"] = '-.'
        lines["R"] = ':'

       
        extension = ".out"
        opt_extension = ".opt.tour"

        files_operations = FilesOperations()

        optimums = files_operations.read_from_optimums_file(optimums_file)

        if "general" in sys.argv or "all" in sys.argv:
            all_files = files_operations.get_dir_files(results_directory)
            all_files = files_operations.size_sort(all_files)
            data = dict()

            for f in all_files:
                print f

                for a in algorithms:
                    data[f, a] = files_operations.read_from_results_file(results_directory + "/" + f + "." + a + extension)

            #dist = Distance(all_files, algorithms, data, optimums, markers, lines)
            #algTime = AlgTime(all_files, algorithms, data, markers, lines)
            #efficiency = Efficiency(all_files, algorithms, data, optimums, markers, lines)
            #steps = Steps(all_files, algorithms, data, markers, lines)
            solNum = SolutionsNum(all_files, algorithms, data, markers, lines)
        
        if "solutions" in sys.argv or "all" in sys.argv:
            all_files = files_operations.get_dir_files(solutions_directory)
            all_files.sort()
            for f in all_files:
                print f

                data = dict()
                data["optimum"] = files_operations.read_from_optimum_path_file(optimums_directory + "/" + f + opt_extension)
                for a in algorithms:
                    data[a] = files_operations.read_from_alg_path_file(solutions_directory + "/" + f + "." + a + extension)

                paths = Paths(f, algorithms + ["optimum"], data)

        if "evaluations" in sys.argv or "all" in sys.argv:
            all_files = files_operations.get_dir_files(evaluations_directory)
            all_files.sort()
            for f in all_files:
                print f

                data = dict()
                for a in algorithms:
                    data[a] = files_operations.read_from_evaluations_file(evaluations_directory + "/" + f + "." + a + extension)

                qualComp = Quality(f, GS, data, optimums[f])
                restarts = Restarts(f, GS, data)

if __name__ == '__main__':
    main()
