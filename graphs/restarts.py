from matplotlib import pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties

class Restarts:

    def __init__(self, name, algorithms, data):
        self.name = name
        self.algorithms = algorithms
        self.data = data
        self.iterations = len(self.data[self.algorithms[0]][0])
        
        self.create_plot(False)
        #self.create_plot(True)

    def create_plot(self, logarithmic):
        fontP = FontProperties()
        fontP.set_size('small')

        fig = plt.figure()
        ax = fig.add_subplot(111)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + 0.2*box.height, box.width, 0.8*box.height])

        if logarithmic == True:
            plt.yscale('log')

        for a in self.algorithms:
            values_mean, values_min = [self.data[a][0][0]], [self.data[a][0][0]]
            elem_sum, elem_num = self.data[a][0][0], 1
            for i in range(1, self.iterations):
                elem_sum += self.data[a][0][i]
                elem_num += 1
                values_mean.append( elem_sum / elem_num )
                values_min.append( self.data[a][0][i] if self.data[a][0][i] < values_min[-1] else values_min[-1] )
    
            ax.plot(values_mean, label="Srednia: " + a, marker='o', linewidth=0, markersize=2, markeredgewidth=0)
            ax.plot(values_min, label="Minimum: " + a, marker='o', linewidth=0, markersize=2, markeredgewidth=0)

        #TODO - random time

        plt.xlabel('Iteracja')
        plt.ylabel('Wartosc funkcji celu')
        
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.13),
            fancybox=True, shadow=True, ncol=2, prop = fontP, numpoints=1)
        
        if logarithmic == False:
            plt.title('Srednie i najlepsze z dotychczas znalezionych rozwiazan - ' + self.name)
            plt.savefig("./pictures/mean_min_" + self.name + ".pdf")
        else:
            plt.title('Srednie i najlepsze z dotychczas znalezionych rozwiazan (log) - ' + self.name)
            plt.savefig("./pictures/mean_min_log_" + self.name + ".pdf")

        #plt.show()

        plt.cla()   # Clear axis
        plt.clf()   # Clear figure
        plt.close() # Close a figure window



