from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
from matplotlib.pyplot import *
from matplotlib.font_manager import FontProperties
import math
from matplotlib.pyplot import cm 

class Efficiency:

    def __init__(self, files, algorithms, data, optimums, markers, lines):
        self.files = files
        self.algorithms = algorithms
        self.data = data
        self.iterations = len(self.data[self.files[0], self.algorithms.keys()[0]])
        self.optimums = optimums
        self.markers = markers
        self.lines = lines
        
        self.create_plots()

    def create_plots(self):
        fontP = FontProperties()
        fontP.set_size('small')

        values = dict()
        for a in self.algorithms.keys():
            values[a] = []

        for f in self.files:
            for a in self.algorithms.keys():
                values[a].append( (self.optimums[f]/np.mean(self.data[f,a][0]))*(1/(math.sqrt(np.mean(self.data[f,a][1]))+1)) )

        ############## plot efficiency

        fig = plt.figure()
        ax = fig.add_subplot(111)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + 0.2*box.height, box.width, 0.8*box.height])

        x = range(len(self.files))
        plt.xticks(x, self.files, rotation=45)
      
        plt.yscale('log')

        color=iter(cm.rainbow(np.linspace(0,1,len(self.algorithms))))
        for a in self.algorithms.keys():
            c=next(color)
            m = self.markers[self.algorithms[a]]
            l = self.lines[self.algorithms[a]]
            plt.errorbar(x, values[a], label=a, marker=m, linestyle=l, color=c, markersize=5)

        plt.xlabel('Algorytm')
        plt.ylabel('Efektownosc [%]')
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.16), fancybox=True, shadow=True, ncol=3, prop = fontP, numpoints=1)
        
        plt.title('Efektywnosc algorytmow (log)')
        plt.savefig("./pictures/efficiency.pdf")
       
        plt.cla()   # Clear axis
        plt.clf()   # Clear figure
        plt.close() # Close a figure window

        ############## plot points
        
        #fig = plt.figure()
        #ax = fig.add_subplot(111)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + 0.2*box.height, box.width, 0.8*box.height])
        grid()

        values = dict()
        for a in self.algorithms.keys():
            values[a] = []
            for f in self.files:
                values[a].append((self.optimums[f]/np.mean(self.data[f,a][0]), 1.0/(np.mean(self.data[f,a][1])+1.0)))
        
        color=iter(cm.rainbow(np.linspace(0,1,len(self.algorithms))))
        for a in self.algorithms.keys():
            c=next(color)
            m = self.markers[self.algorithms[a]]
            ax.plot( *zip(*(values[a])), label=a, marker=m, color=c, markersize=10.0, linestyle="None", clip_on=False)
       
        #plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.08),
        #    fancybox=True, shadow=True, ncol=3, prop = fontP, numpoints=1)

        plt.xlabel('Ocena jakosci [%]')
        plt.ylabel('Ocena czasu [%]')
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.16), fancybox=True, shadow=True, ncol=3, prop = fontP, numpoints=1)
            
        plt.title('Efektywnosc algorytmow')
        plt.savefig("./pictures/efficiency_points.pdf")

        plt.cla()   # Clear axis
        plt.clf()   # Clear figure
        plt.close() # Close a figure window

