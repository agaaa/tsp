from matplotlib import pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties

class Steps:

    def __init__(self, files, algorithms, data, markers, lines):
        self.files = files
        self.algorithms = algorithms
        self.data = data
        self.iterations = len(self.data[self.files[0], self.algorithms.keys()[0]])
        self.markers = markers
        self.lines = lines
        
        self.create_plots()

    def create_plots(self):
        fontP = FontProperties()
        fontP.set_size('small')

        values_mean, values_std = dict(), dict()
        for a in self.algorithms.keys():
            values_mean[a] = []
            values_std[a] = []

        for f in self.files:
            for a in self.algorithms.keys():
                if self.algorithms[a] == "GS":
                    values_mean[a].append( np.mean(self.data[f,a][2]) )
                    values_std[a].append( np.std(self.data[f,a][2]) )

        ############## plot mean

        fig = plt.figure()
        ax = fig.add_subplot(111)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + 0.2*box.height, box.width, 0.8*box.height])

        x = range(len(self.files))
        plt.xticks(x, self.files, rotation=45)
      
        plt.yscale('log')

        for a in self.algorithms.keys():
            if self.algorithms[a] == "GS":
                m = self.markers[self.algorithms[a]]
                l = self.lines[self.algorithms[a]]
                plt.errorbar(x, values_mean[a], values_std[a], label=a, marker=m, linestyle=l, markersize=2)

        plt.xlabel('Algorytm')
        plt.ylabel('Liczba krokow')
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.16), fancybox=True, shadow=True, ncol=2, prop = fontP, numpoints=1)
        
        plt.title('Liczba krokow algorytmow GS (log)')
        plt.savefig("./pictures/steps.pdf")
       
        plt.cla()   # Clear axis
        plt.clf()   # Clear figure
        plt.close() # Close a figure window

