import sys

class Paths:

    def __init__(self, name, algorithms, data):
        self.name = name
        self.data = data
        self.algorithms = algorithms
        self.alg_num = len(self.algorithms)
        self.size = len(self.data[self.algorithms[0]])

        values = self.calculate()
        text = self.print_similarities(values)
        self.save("tables/" + self.name, text)

    def calculate(self):
        values = [ [ 1.0 for i in range(self.alg_num) ] for j in range(self.alg_num) ]
        for i in range(self.alg_num):
            for j in range(i+1, self.alg_num):
                val = self.compare(self.data[self.algorithms[i]], self.data[self.algorithms[j]])
                values[i][j] = val
                values[j][i] = val
        return values

    def compare(self, path1, path2):
        same = 0
        for i in range(self.size):
            if path1[(i+1)%self.size] in self.get_neighbours(path1[i], path2):
                same += 1
        return 1.0*same/self.size

    def get_neighbours(self, val, path):
        for i in range(self.size):
            if path[i] == val:
                return path[(i+self.size-1)%self.size], path[(i+1)%self.size]
        return None
       
    def print_similarities(self, values):
        result = ""

        for a in self.algorithms:
            result += " & "
            result += a
            #sys.stdout.write(" & ")
            #sys.stdout.write(a)
        result += " \\\\\n"
        #print r" \\"

        for i in range(self.alg_num):
            result += self.algorithms[i]
            #sys.stdout.write(self.algorithms[i])
            for j in range(self.alg_num):
                result += " & "
                result += str(round(values[i][j],3))
                #sys.stdout.write(" & ")
                #sys.stdout.write(str(values[i][j]))
            result += " \\\\\n"
            #print r" \\"

        return result

    def save(self, path, text):
        target = open(path, 'w')
        target.write(text)
        target.close()

