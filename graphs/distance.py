from matplotlib import pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties
from matplotlib.pyplot import cm 

class Distance:

    def __init__(self, files, algorithms, data, optimums, markers, lines):
        self.files = files
        self.algorithms = algorithms
        self.data = data
        self.iterations = len(self.data[self.files[0], self.algorithms.keys()[0]])
        self.optimums = optimums
        self.markers = markers
        self.lines = lines

        self.create_plots()

    def create_plots(self):
        fontP = FontProperties()
        fontP.set_size('small')

        values_min, values_mean, values_max, values_std = dict(), dict(), dict(), dict()
        for a in self.algorithms:
            values_min[a] = []
            values_mean[a] = []
            values_max[a] = []
            values_std[a] = []

        for f in self.files:
            for a in self.algorithms.keys():
                values_min[a].append( min( [(v-self.optimums[f])/self.optimums[f] for v in self.data[f,a][0]]) )
                values_mean[a].append( np.mean( [(v-self.optimums[f])/self.optimums[f] for v in self.data[f,a][0]]) )
                values_max[a].append( max( [(v-self.optimums[f])/self.optimums[f] for v in self.data[f,a][0]]) )
                values_std[a].append( np.std( [(v-self.optimums[f])/self.optimums[f] for v in self.data[f,a][0]]) )
                #print a, values_min[a]


        ############## plot min

        fig = plt.figure()
        ax = fig.add_subplot(111)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + 0.2*box.height, box.width, 0.8*box.height])

        x = range(len(self.files))
        plt.xticks(x, self.files, rotation=45)
      
        plt.yscale('log')

        color=iter(cm.rainbow(np.linspace(0,1,len(self.algorithms))))
        for a in sorted(self.algorithms.keys()):
            c=next(color)
            m = self.markers[self.algorithms[a]]
            l = self.lines[self.algorithms[a]]
            plt.plot(values_min[a], label=a, marker=m, linestyle=l, color=c, clip_on=False)

        plt.xlabel('Algorytm')
        plt.ylabel('Odleglosc')
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.16), fancybox=True, shadow=True, ncol=3, prop = fontP, numpoints=1)
        
        plt.title('Minimalna odleglosc od optimum (log)')
        plt.savefig("./pictures/distance_min.pdf")
       
        self.clear_plot()

        ############## plot mean
        
        fig = plt.figure()
        ax = fig.add_subplot(111)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + 0.2*box.height, box.width, 0.8*box.height])

        x = range(len(self.files))
        plt.xticks(x, self.files, rotation=45)
      
        plt.yscale('log')

        color=iter(cm.rainbow(np.linspace(0,1,len(self.algorithms))))
        for a in sorted(self.algorithms.keys()):
            c=next(color)
            m = self.markers[self.algorithms[a]]
            l = self.lines[self.algorithms[a]]
            plt.errorbar(x, values_mean[a], values_std[a], label=a, marker=m, linestyle=l, color=c, markersize=2, clip_on=False)

        plt.xlabel('Algorytm')
        plt.ylabel('Odleglosc')
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.16), fancybox=True, shadow=True, ncol=3, prop = fontP, numpoints=1)
        
        plt.title('Srednia odleglosc od optimum (log)')
        plt.savefig("./pictures/distance_mean.pdf")
       
        self.clear_plot()

        ############## plot max

        fig = plt.figure()
        ax = fig.add_subplot(111)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + 0.2*box.height, box.width, 0.8*box.height])

        x = range(len(self.files))
        plt.xticks(x, self.files, rotation=45)
      
        plt.yscale('log')

        color=iter(cm.rainbow(np.linspace(0,1,len(self.algorithms))))
        for a in sorted(self.algorithms.keys()):
            c=next(color)
            m = self.markers[self.algorithms[a]]
            l = self.lines[self.algorithms[a]]
            plt.plot(values_max[a], label=a, marker=m, linestyle=l, color=c, clip_on=False)

        plt.xlabel('Algorytm')
        plt.ylabel('Odleglosc')
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.16), fancybox=True, shadow=True, ncol=3, prop = fontP, numpoints=1)
        
        plt.title('Maksymalna odleglosc od optimum (log)')
        plt.savefig("./pictures/distance_max.pdf")
       
        self.clear_plot()


    def clear_plot(self):
        plt.cla()   # Clear axis
        plt.clf()   # Clear figure
        plt.close() # Close a figure window
