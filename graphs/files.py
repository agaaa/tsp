import os.path
from os import listdir
from os.path import isfile, join

class FilesOperations:

    def __init__(self):
        pass

    def read_from_results_file(self, file_name):
        results, time, steps, evaluatedSolutions = [], [], [], []

        input_file = open(file_name)
        try:
            for line in input_file:
                values = line.split()
                results.append(float(values[0]))
                time.append(float(values[1]))
                if(len(values) > 2):
                    evaluatedSolutions.append(int(values[2]))
                if(len(values) > 3):
                    steps.append(int(values[3]))
        finally:
            input_file.close()

        return results, time, steps, evaluatedSolutions

    def read_from_optimums_file(self, file_name):
        optimums = dict()

        input_file = open(file_name)
        try:
            for line in input_file:
                values = line.split()
                optimums[values[0]] = float(values[2])

        finally:
            input_file.close()

        return optimums

    def read_from_optimum_path_file(self, file_name):
        path = []

        input_file = open(file_name)
        try:
            line = input_file.readline()
            while line != "TOUR_SECTION\n":
                line = input_file.readline()

            for line in input_file:
                if line != "EOF\n":
                    path.append(int(line)-1)
        finally:
            input_file.close()

        return path

    def read_from_alg_path_file(self, file_name):
        path = []

        input_file = open(file_name)
        try:
            for line in input_file:
                path.append(int(line))
        finally:
            input_file.close()

        return path

    def read_from_evaluations_file(self, file_name):
        final, initial = [], []

        input_file = open(file_name)
        try:
            for line in input_file:
                values = line.split()
                final.append(float(values[0]))
                if len(values) > 1:
                    initial.append(float(values[1]))
        finally:
            input_file.close()

        return final, initial

    def check_if_path_exists(self, path):
        if os.path.exists(path):
            return True
        return False

    def create_directory(self, directory):
        os.makedirs(directory)

    def get_dir_files(self, directory):
        files = []
        if self.check_if_path_exists(directory) == True:
            files = list(set( f.split(".")[0] for f in listdir(directory) if isfile(join(directory, f)) ))
        return files
             
       
    def size_sort(self, files_list):
        files_sizes = dict()
        for f in files_list:
            i = 1
            while f[-i] >= '0' and f[-i] <= '9':
                i+=1
            files_sizes[int(f[-i+1:])] = f
        sorted_files = []
        for k in sorted(files_sizes):
            sorted_files.append(files_sizes[k])
        return sorted_files

        

