from matplotlib import pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties
from matplotlib.pyplot import cm 

class AlgTime:

    def __init__(self, files, algorithms, data, markers, lines):
        self.files = files
        self.algorithms = algorithms
        self.data = data
        self.iterations = len(self.data[self.files[0], self.algorithms.keys()[0]])
        self.markers = markers
        self.lines = lines
        
        self.create_plots()

    def create_plots(self):
        fontP = FontProperties()
        fontP.set_size('small')

        values_mean, values_std = dict(), dict()
        for a in self.algorithms.keys():
            values_mean[a] = []
            values_std[a] = []

        for f in self.files:
            for a in self.algorithms.keys():
                if a != "random":
                    values_mean[a].append( np.mean(self.data[f,a][1]) )
                    values_std[a].append( np.std(self.data[f,a][1]) )
            values_mean["random"].append( np.mean( [ values_mean["greedy-reverse"][-1], values_mean["greedy-swap"][-1], values_mean["steepest-reverse"][-1], values_mean["steepest-reverse"][-1] ] ) )
            values_std["random"].append( np.std(self.data[f,a][1]) )

        ############## plot mean

        fig = plt.figure()
        ax = fig.add_subplot(111)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + 0.2*box.height, box.width, 0.8*box.height])

        x = range(len(self.files))
        plt.xticks(x, self.files, rotation=45)
      
        plt.yscale('log')

        color=iter(cm.rainbow(np.linspace(0,1,len(self.algorithms))))
        for a in self.algorithms:
            c=next(color)
            m = self.markers[self.algorithms[a]]
            l = self.lines[self.algorithms[a]]
            plt.errorbar(x, values_mean[a], values_std[a], label=a, marker=m, linestyle=l, color=c, markersize=2)

        plt.xlabel('Algorytm')
        plt.ylabel('Czas [ms]')
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.16), fancybox=True, shadow=True, ncol=3, prop = fontP, numpoints=1)
        
        plt.title('Sredni czas dzialania algorytmow (log)')
        plt.savefig("./pictures/time.pdf")
       
        plt.cla()   # Clear axis
        plt.clf()   # Clear figure
        plt.close() # Close a figure window

